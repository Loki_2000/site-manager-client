## Site Manager

**First version of the Site Manager application(client-side) written in Backbone.js and Marionette.js.** 

The concept of this application is to provide the various sites of a large construction company with a means to post left over materials for collection and storage by a central Plant Yard.
Once collected and stored by the Plant Yard, the items will become visible on the materials database. This database displayed within the application enables sites to order a delivery of a given
material to any site location thus recycling any leftover construction materials. A user can view a list of his open orders for both collection and delivery.

In order to communicate orders and their status between Site staff and Plant Yard staff there is a further client side application called Plant Manager. (see plant-manager-client).
Both applications use a common backend written in express.js and using a mongoDB database. (See "site-manager-srv") 

---

## Steps to Setup Locally

**NOTE: Currently the application runs on node v6.9.1. Errors may occur on other versions. (*Needs to be updated to v8.X*)**


1. Install NVM ```curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.6/install.sh | bash```

2. Install correct version of node ```nvm install 6.9.1 && nvm use 6.9.1```

1. Clone Repositiory 

2. Navigate to Repository

3. Run ```npm install```

4. Ensure gulp is installed globally. ```npm install -g gulp```

5. Run ```gulp develop```

*Server should run on port 3000*

*NOTE: Default API URL is "htp://localhost:8081/api" - For this to work you must have site-manager-srv installed locally and running* 
