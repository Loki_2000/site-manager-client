var $          = require('jquery')
	Marionette = require('backbone.marionette'),
    ItemView   = require('./item'),
    Template   = require('./templates/filter.hbs'),
    BlockTypes = require('../../blockwork_class/collection'),
    BlockType  = require('../../blockwork_class/model');

List = Marionette.CollectionView.extend({
	tagname : 'div',
	childView : ItemView,
	initialize : function(){
		$('#filter-view').show();

		var self = this;
		var params = {
			status : 'collected',
			material : 'block'
		}

		this.collection.fetch({
			data: $.param(params)
		}).done(function(response){
			if (response.length) {
				self.showFilter();
			} else {
				var model = new Backbone.Model();
				model.set({
					noData: true,
					text: 'No items available'
				});
				self.collection.add(model);
			}
		})
		.fail(function(response){
		})
	},
	showFilter : function(){
		this.filters = new BlockTypes()
		this.filters.on('change', this.filterByDensity, this);
		this.filters.on('change:size', this.filterBySize, this);
		var view = new FilterView({collection : this.filters})
		window.App.filterRegion.show(view);
	},
	filterByDensity : function(density){
		
		this.collection.each(function(model){
			model.set({
				filter : false,
				'filter-size' : false
			});

			if(density){
				if(model.get('density') !== density){
					model.set({filter : true});
				}// end if
			} else {
				model.set({filter : false});
			}// end if
		});

		this.render(this.collection)
	},
	filterBySize : function(size){
		this.collection.each(function(model){
			
			model.set({'filter-size' : false});

			if(size){
				if(model.get('blocksize') !== size){
					model.set({'filter-size' : true});
				}// end if
			}// end if
		});

		this.render(this.collection)
	},
	destroy : function(){
		$('#filter-view').hide();
	}
});

var FilterView = Marionette.ItemView.extend({
	id : 'filter-view',
	tagName : 'div',
	className : 'filter-container',
	template : Template,
	events : {
		'change select#density-filter' : 'filterByDensity',
		'change select#size-filter' : 'filterBySize',
	},
	initialize : function(){
		var self = this;
		// this.collection.fetch().done(function(response){
		// 	self.getAllTypes();
		// });
        this.model = new Backbone.Model();
        this.model.set(window.App.instance.get('blockTypes').toJSON()[0]);
	},
	filterByDensity : function(e){
		
		var selectedDensity = $(e.currentTarget).val()

	    this.collection.trigger('change', selectedDensity);
	},
	filterBySize : function(e){
		var selectedSize = $(e.currentTarget).val();

		this.collection.trigger('change:size', selectedSize)
	}
	
});

module.exports = List;