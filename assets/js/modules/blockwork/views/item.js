var $ = require('jquery')
	Marionette = require('backbone.marionette'),
	Backbone = require('backbone'),
    Template = require('./templates/item.hbs');

Item = Marionette.ItemView.extend({
	id : 'block_item-view',
	tagName : 'div',
	className : 'list_item',
	template : Template,
	events : {
		'click div.block-item' : 'orderItem',
		'click button.info' : 'showInfo',
		'click div.other-info' : 'hideInfo'
	},
	initialize : function(){
		
	},
	onShow : function(){

	},
	showInfo : function(e){
		e.stopPropagation();
		this.$el.find('.other-info').show();
	},
	hideInfo : function(e){
		e.stopPropagation();
		this.$el.find('.other-info').hide();
	},
	orderItem : function(){
		Backbone.history.navigate('order/block/'+this.model.id, {trigger:true});
	}

});

module.exports = Item;