var $          = require('jquery')
	Marionette = require('backbone.marionette'),
	Backbone   = require('backbone'),
    Template   = require('./templates/incoming.hbs');

var Item = Marionette.ItemView.extend({
	id         : 'order-view',
	tagName    : 'div',
	className  : 'list_item',
	template   : Template,
	initialize : function(){
		
	}
});

List = Marionette.CollectionView.extend({
	tagname    : 'div',
	className  : 'orders-view',
	childView  : Item,
	initialize : function(){
		var self = this;
		var params = {
			ordered_by : window.App.instance.get('user').get('username')
		}
		this.collection.fetch({
			data : $.param(params)
		}).done(function(response){
			if (!response.length) {
				var model = new Backbone.Model();
				model.set({
					noData: true,
					text: 'No items ordered'
				});
				self.collection.add(model);
			}
		});
	}
});

module.exports = List;

