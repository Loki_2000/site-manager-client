var $        = require('jquery'),
	Backbone = require('backbone'),
	moment = require('moment'),
	Model    = require('../steel/model');
	Backbone.$ = $;

Incoming = Backbone.Collection.extend({
	model : Model,
	url : function(){
		return window.App.apiURL + '/account/incoming';
	},
	comparator: function(item){
		if (item.get('date_req')) {
			return moment(item.get('date_req').toString(), 'DD-MM-YYYY').unix()*-1;
		}
	}
});

Outgoing = Backbone.Collection.extend({
	model : Model,
	url : function(){
		return window.App.apiURL + '/account/outgoing';
	},
	comparator: function(item){
		if (item.get('date_col')) {
			return moment(item.get('date_col').toString(), 'DD-MM-YYYY').unix()*-1;
		}
	}
});

module.exports = {
	Incoming : Incoming,
	Outgoing : Outgoing
};