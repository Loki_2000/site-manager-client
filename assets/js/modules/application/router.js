var $          = require('jquery'),
	Backbone   = require('backbone'),
	Controller = require('./controller');

Backbone.$ = $;

var Router = Backbone.Router.extend({
	routes : {
		'home'   : 'navHome',
		'add'    : 'addMaterials',
		'search' : 'searchMaterials',
		'admin'  : 'showAdminOptions'
	},
	initialize : function(){
		this.controller = new Controller();
	},
	navHome : function(){
		if(!localStorage.getItem('token'))
			return Backbone.history.navigate('', {trigger: true})
		else
			this.controller.navHome();
	},
	addMaterials : function(){
		if(!localStorage.getItem('token'))
			return Backbone.history.navigate('', {trigger: true})
		else
			this.controller.addMaterials();
	},
	searchMaterials : function(){
		if(!localStorage.getItem('token'))
			return Backbone.history.navigate('', {trigger: true})
		else
			this.controller.searchMaterials();
	},
	showAdminOptions: function(){
		if(!localStorage.getItem('token') || !window.App.instance.get('user').get('isAdmin'))
			return Backbone.history.navigate('', {trigger: true})
		else
			this.controller.showAdminOptions();
	}
});

module.exports = Router;