var $          = require('jquery'),
	Backbone   = require('backbone'),
	User       = require('../user/module'),
	Sites      = require('../sites/module'),
	SteelTypes = require('../steel_class/module'),
	BrickTypes = require('../brickwork_class/module'),
	BlockTypes = require('../blockwork_class/module'),
	RebarTypes = require('../rebar_class/module'),
	TimberTypes= require('../timber_class/module');
	

Backbone.$ = $;

var INITIAL_DATA = ['steelTypes', 'brickTypes', 'blockTypes', 'rebarTypes', 'timberTypes', 'sites'];

Model = Backbone.Model.extend({
	defaults : {
		user       : new User.Model(),
		steelTypes : new SteelTypes.Collection(),
		brickTypes : new BrickTypes.Collection(),
		blockTypes : new BlockTypes.Collection(),
		rebarTypes : new RebarTypes.Collection(),
		timberTypes: new TimberTypes.Collection(),
		sites      : new Sites.Collection()
	},
	initialize : function(){
		var _this = this;

		INITIAL_DATA.forEach(function(collection){
			_this.defaults[collection].fetch().done(function(res){
				
			});
		})
	}
});

module.exports = Model;