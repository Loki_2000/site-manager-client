var $          = require('jquery'),
	Marionette = require('backbone.marionette'),
	Backbone   = require('backbone'),
    Template   = require('./templates/search.hbs');

Search = Marionette.ItemView.extend({
	id        : 'material-types-view',
	tagName   : 'div',
	className : 'material-types-el',
	template  : Template,
	events : {
		'click div#search_steel' : 'searchSteel',
		'click div#search_brick' : 'searchBrick',
		'click div#search_block' : 'searchBlock',
		'click div#search_rebar' : 'searchRebar',
		'click div#search_timber' : 'searchTimber',
		'click div#search_other' : 'searchOther'
	},
	initialize : function(){
		
	},
	searchSteel : function(){
		Backbone.history.navigate('search/steel', {trigger : true});
	},
	searchBrick : function(){
		Backbone.history.navigate('search/brick', {trigger : true});
	},
	searchBlock : function(){
		Backbone.history.navigate('search/block', {trigger : true});
	},
	searchRebar : function(){
		Backbone.history.navigate('search/rebar', {trigger : true});
	},
	searchTimber : function(){
		Backbone.history.navigate('search/timber', {trigger : true});
	},
	searchOther : function(){
		Backbone.history.navigate('search/other', {trigger : true});
	}

});

module.exports = Search;