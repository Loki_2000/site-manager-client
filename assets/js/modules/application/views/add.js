var $          = require('jquery'),
	Marionette = require('backbone.marionette'),
	Backbone   = require('backbone'),
    Template   = require('./templates/add.hbs');

Add = Marionette.ItemView.extend({
	id        : 'material-types-view',
	tagName   : 'div',
	className : 'material-types-el',
	template  : Template,
	events : {
		'click div#add_steel' : 'addSteel',
		'click div#add_brick' : 'addBrick',
		'click div#add_block' : 'addBlock',
		'click div#add_rebar' : 'addRebar',
		'click div#add_timber': 'addTimber',
		'click div#add_other' : 'addOther'
	},
	initialize : function(){	
		
	},
	addSteel : function(){
		Backbone.history.navigate('add/steel', {trigger : true});
	},
	addBrick : function(){
		Backbone.history.navigate('add/brick', {trigger : true});
	},
	addBlock : function(){
		Backbone.history.navigate('add/block', {trigger : true});
	},
	addTimber : function(){
		Backbone.history.navigate('add/timber', {trigger : true});
	},
	addRebar : function(){
		Backbone.history.navigate('add/rebar', {trigger : true});
	},
	addOther : function(){
		Backbone.history.navigate('add/other', {trigger : true});
	}

});

module.exports = Add;