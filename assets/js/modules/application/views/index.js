var $          = require('jquery'),
	Marionette = require('backbone.marionette'),
	io         = require('socket.io-client'),
	Backbone   = require('backbone'),
    Template   = require('./templates/index.hbs');

Index = Marionette.ItemView.extend({
	id        : 'home-view',
	tagName   : 'div',
	className : 'home-view',
	template  : Template,
	events : {
		'click div#search' : 'searchMaterials',
		'click div#add'    : 'addMaterials',
		'click #goToAdmin' : 'showAdmin'
	},
	initialize : function(){
		if(localStorage.getItem('token'))
			this.model.get('user').loggedIn();
	},
	onShow: function(){
		if(window.App.instance.get('user').get('isAdmin') === false)
			this.$el.find('#goToAdmin').hide();


		this.setupWebsocket();
	},
	searchMaterials : function(){
		Backbone.history.navigate('search', {trigger : true})
	},
	setupWebsocket: function() {
		// window.App.instance.socket = io('ws://intense-thicket-2598.herokuapp.com', {credentials: 'allow'});
		// window.App.instance.socket.on('newmsg', function(data) {
		// 	console.log(data);
		// 	alert(data.message.density)
		// })
		// window.App.instance.socket.on('single', function(data) {
		// 	console.log(data);
		// 	alert(data.description);
		// })
	},
	addMaterials : function(){
		// window.App.instance.socket.emit('new item added', {});
		console.log('fghj')
		Backbone.history.navigate('add', {trigger : true})
	},
	showAdmin: function(){
		Backbone.history.navigate('admin', {trigger : true})
	}
});

module.exports = Index;