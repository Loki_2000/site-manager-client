var $          = require('jquery')
	Marionette = require('backbone.marionette'),
    ItemView   = require('./item'),
    Template   = require('./templates/filter.hbs'),
    RebarTypes = require('../../rebar_class/collection'),
    RebarType  = require('../../rebar_class/model');

List = Marionette.CollectionView.extend({
	tagname : 'div',
	childView : ItemView,
	initialize : function(){
		var self = this;
		var params = {
			status : 'collected',
			material : 'other'
		}

		this.collection.fetch({
			data: $.param(params)
		}).done(function(response){
			if (!response.length) {
				var model = new Backbone.Model();
				model.set({
					noData: true,
					text: 'No items available'
				});
				self.collection.add(model);
			}
		})
		.fail(function(response){
		})
	},
	// showFilter : function(){
	// 	this.filters = new RebarTypes()
	// 	this.filters.on('change', this.filterByDiameter, this);
	// 	var view = new FilterView({collection : this.filters})
	// 	window.App.filterRegion.show(view);
	// },
	// filterByDiameter : function(diameter){
		
	// 	this.collection.each(function(model){
	// 		model.set({
	// 			filter : false,
	// 			'filter-size' : false
	// 		});

	// 		if(diameter){
	// 			if(model.get('diameter') !== diameter){
	// 				model.set({filter : true});
	// 			}// end if
	// 		} else {
	// 			model.set({filter : false});
	// 		}// end if
	// 	});

	// 	this.render(this.collection)
	// },
	// filterBySize : function(size){
	// 	this.collection.each(function(model){
			
	// 		model.set({'filter-size' : false});

	// 		if(size){
	// 			if(model.get('rebarsize') !== size){
	// 				model.set({'filter-size' : true});
	// 			}// end if
	// 		}// end if
	// 	});

	// 	this.render(this.collection)
	// },
	destroy : function(){
		$('#filter-view').hide();
	}
});

// var FilterView = Marionette.ItemView.extend({
// 	id : 'filter-view',
// 	tagName : 'div',
// 	className : 'filter-container',
// 	template : Template,
// 	events : {
// 		'change select#diameter-filter' : 'filterByDiameter'
// 	},
// 	initialize : function(){
// 		var self = this;
// 		// this.collection.fetch().done(function(response){
// 		// 	self.getAllTypes();
// 		// });
//         this.model = new Backbone.Model();
//         this.model.set(window.App.instance.get('rebarTypes').toJSON()[0]);
// 	},
// 	filterByDiameter : function(e){
		
// 		var selectedDiameter = $(e.currentTarget).val()

// 	    this.collection.trigger('change', selectedDiameter);
// 	}
	
// });

module.exports = List;