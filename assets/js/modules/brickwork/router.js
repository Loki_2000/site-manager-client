var $ 		   = require('jquery'),
	Backbone   = require('backbone'),
	Controller = require('./controller'),
	Module     = require('./module');

Backbone.$ = $;

Router = Backbone.Router.extend({
	routes : {
		'add/brick'       : 'add',
		'search/brick' 	  : 'search',
		'order/brick/:id' : 'order',
		'edit/brick/:id'  : 'edit',
		'view/brick/:id'  : 'view'
	},
	initialize : function(){
		this.controller = new Controller();
	},
	add : function(){
		if(!localStorage.getItem('token'))
			return Backbone.history.navigate('', {trigger: true})
		else
			this.controller.add();
	},
	search : function(){
		if(!localStorage.getItem('token'))
			return Backbone.history.navigate('', {trigger: true})
		else
			this.controller.search();
	},
	order : function(id){
		if(!localStorage.getItem('token'))
			return Backbone.history.navigate('', {trigger: true})
		else
			this.controller.order(id);
	},
    edit : function(id){
    	if(!localStorage.getItem('token'))
			return Backbone.history.navigate('', {trigger: true})
		else
        	this.controller.edit(id);
    },
    view : function(id){
    	if(!localStorage.getItem('token'))
			return Backbone.history.navigate('', {trigger: true})
		else
        	this.controller.view(id);
    }
});

module.exports = Router;