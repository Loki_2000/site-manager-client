var $        = require('jquery'),
	Backbone = require('backbone');
	Backbone.$ = $;

Model = Backbone.Model.extend({
	defaults : {
		type       : '',
		strength   : '',
		finish     : '',
		palletNo   : null,
		comments   : '',
		added_by   : '',
		date_added : '',
		date_col   : '',
		available  : false
	},
	initialize : function(){
		
	},
	parse : function(response){
		response.id = response._id;
		return response;
	},
	urlRoot : function(){
		// 'http://localhost:8080/api/add-steel'
		var url = window.App.apiURL + '/'+this.get('extension');
		return url;
	},
	sync : function(method, model, options){
		if(method==='read' || method==='delete' || method==='update'){
		   options.url =  window.App.apiURL + '/'+this.get('extension') + '/' + this.id; 
		}
		else if(method==='create'){
			options.url =  window.App.apiURL + '/'+this.get('extension'); 
		}

		return Backbone.sync(method, model, options);
	}
});

module.exports = Model;