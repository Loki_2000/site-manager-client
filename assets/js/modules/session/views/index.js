var $          = require('jquery'),
	Marionette = require('backbone.marionette'),
	Backbone   = require('backbone'),
    Template   = require('./templates/index.hbs');

Backbone.$ = $;

Index = Marionette.ItemView.extend({
	id        : 'login-view',
	tagName   : 'div',
	className : 'login-view',
	template  : Template,
	events : {
		'input #username'        : 'validateUsername',
		'input #password'        : 'validateUsername',
		'click #submit_password' : 'authenticateUser',
	},
	initialize : function(){
		if(localStorage.getItem('token')) {
			Backbone.history.navigate('home', {trigger : true});
		}

	},
	validateUsername : function(){
	},
	authenticateUser : function(){
		var data = {
			username : $('#username').val(),
			password : $('#password').val()
		};
		
		var self = this;

		this.model.fetch({data:data})
		.done(function(response){
			if(!response.success){
				self.$el.find('.error').append('<h3>'+response.message.title+'</h3><p>'+response.message.text+'</p>');
				self.$el.find('.error').show();
				setTimeout(function(){
					self.$el.find('.error').fadeOut();
					self.$el.find('.error').empty();
				}, 3000);
				return;
			}
			else{
				localStorage.setItem('token', response.token);
				Backbone.history.navigate('home', {trigger : true});
				$.ajaxSetup({headers: {'x-access-token': localStorage.getItem('token') || ''}})
			}
		})
		.fail(function(err){
			console.error(err)
			self.$el.find('.error').append('<h3>'+err.statusText+'!</h3><p>Please fill in all fields</p>');
				self.$el.find('.error').show();
				setTimeout(function(){
					self.$el.find('.error').fadeOut();
					self.$el.find('.error h3').empty();
					self.$el.find('.error p').empty();
				}, 3000);
		});
	}

});

module.exports = Index;