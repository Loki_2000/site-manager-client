var $          = require('jquery'),
	Backbone   = require('backbone'),
	Controller = require('./controller');

Backbone.$ = $;

var Router = Backbone.Router.extend({
	routes : {
		'users'   : 'showUsers',
		'user/add': 'addUser'
	},
	initialize : function(){
		this.controller = new Controller();
	},
	showUsers: function(){
		if(!localStorage.getItem('token') || !window.App.instance.get('user').get('isAdmin'))
			return Backbone.history.navigate('', {trigger: true})
		else
			this.controller.showUsers();
	},
	addUser: function(){
		if(!localStorage.getItem('token') || !window.App.instance.get('user').get('isAdmin'))
			return Backbone.history.navigate('', {trigger: true})
		else
			this.controller.addUser();
	}
});

module.exports = Router;