var $          = require('jquery')
	Marionette = require('backbone.marionette'),
    ItemView   = require('./item'),
    Template   = require('./templates/filter.hbs'),
    TimberTypes = require('../../timber_class/collection'),
    TimberType  = require('../../timber_class/model');

List = Marionette.CollectionView.extend({
	tagname : 'div',
	childView : ItemView,
	initialize : function(){
		$('#filter-view').show();

		var self = this;
		var params = {
			status : 'collected',
			material : 'timber'
		};

		this.collection.fetch({
			data: $.param(params)
		}).done(function(response){
			if (response.length) {
				self.showFilter();
			} else {
				var model = new Backbone.Model();
				model.set({
					noData: true,
					text: 'No items available'
				});
				self.collection.add(model);
			}
		})
		.fail(function(response){
		})
	},
	showFilter : function(){
		this.filters = new TimberTypes()
		this.filters.on('change', this.filterByType, this);
		this.filters.on('change:grade', this.filterByGrade, this);
		var view = new FilterView({collection : this.filters})
		window.App.filterRegion.show(view);
	},
	filterByType : function(type){
		
		this.collection.each(function(model){
			model.set({
				filter : false,
				'filter-size' : false
			});

			if(type){
				if(model.get('type') !== type){
					model.set({filter : true});
				}// end if
			} else {
				model.set({filter : false});
			}// end if
		});

		this.render(this.collection)
	},
	filterByGrade : function(grade){
		this.collection.each(function(model){
			
			model.set({'filter-grade' : false});
			if(grade){
				if(model.get('grade') !== grade){
					model.set({'filter-grade' : true});
				}// end if
			}// end if
		});

		this.render(this.collection)
	},
	destroy : function(){
		$('#filter-view').hide();
	}
});

var FilterView = Marionette.ItemView.extend({
	id : 'filter-view',
	tagName : 'div',
	className : 'filter-container',
	template : Template,
	events : {
		'change select#type-filter' : 'filterByType',
		'change select#grade-filter' : 'filterByGrade',
	},
	initialize : function(){
		var self = this;
		// this.collection.fetch().done(function(response){
		// 	self.getAllTypes();
		// });
        this.model = new Backbone.Model();
        this.model.set(window.App.instance.get('timberTypes').toJSON()[0]);
	},
	filterByType : function(e){
		
		var selectedType = $(e.currentTarget).val()

	    this.collection.trigger('change', selectedType);
	},
	filterByGrade : function(e){
		var selectedGrade = $(e.currentTarget).val();

		this.collection.trigger('change:grade', selectedGrade)
	}
	
});

module.exports = List;