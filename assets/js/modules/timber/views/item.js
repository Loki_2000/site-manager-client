var $ = require('jquery')
	Marionette = require('backbone.marionette'),
	Backbone = require('backbone'),
    Template = require('./templates/item.hbs');

Item = Marionette.ItemView.extend({
	id : 'timber_item-view',
	tagName : 'div',
	className : 'list_item',
	template : Template,
	events : {
		'click div.timber-item' : 'orderItem',
		'click button.info' : 'showInfo',
		'click div.other-info' : 'hideInfo'
	},
	initialize : function(){
		
	},
	onShow : function(){

	},
	showInfo : function(e){
		e.stopPropagation();
		this.$el.find('.other-info').show();
	},
	hideInfo : function(e){
		e.stopPropagation();
		this.$el.find('.other-info').hide();
	},
	orderItem : function(){
		Backbone.history.navigate('order/timber/'+this.model.id, {trigger:true});
	}

});

module.exports = Item;