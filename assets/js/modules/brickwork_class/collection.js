var $        = require('jquery'),
	Backbone = require('backbone'),
	Model = require('./model');

Backbone.$ = $;

Collection = Backbone.Collection.extend({
	initialize : function(){
		
	},
	model : Model,
	url : function(){
		return window.App.apiURL + '/brick_types'
	}

});

module.exports = Collection;