var $        = require('jquery'),
	Backbone = require('backbone');
	
Backbone.$ = $;

Model = Backbone.Model.extend({
	defaults : {
		types    : [],
		strengths : [],
		finishes   : []
	},
	initialize : function(options){

	},
	parse : function(response){
		response.id = response._id;
		return response;
	},
	urlRoot : window.App.apiURL + '/brick_types'
});

module.exports = Model;