'use strict';

var $          = require('jquery'),
	Marionette = require('backbone.marionette'),
	Backbone   = require('backbone');
	

Backbone.$ = $;

// Set Remote and Local URLs
var remote_url = 'https://intense-thicket-2598.herokuapp.com/api';
var local_url  = 'http://localhost:8081/api';

var App = window.App = new Marionette.Application();

// Required Modules and files for App Initialization
var Application   = require('./modules/application/module'),
	AppRouter     = require('./modules/application/router'),
	SteelRouter   = require('./modules/steel/router'),
	BrickRouter   = require('./modules/brickwork/router'),
	BlockRouter   = require('./modules/blockwork/router'),
	RebarRouter   = require('./modules/rebar/router'),
	TimberRouter  = require('./modules/timber/router'),
	OtherRouter   = require('./modules/other/router'),
	UserRouter    = require('./modules/user/router'),
	SessionRouter = require('./modules/session/router'),
	AdminRouter   = require('./modules/admin/router');

// Setup Application
App.views    = {};
App.data     = {};
App.apiURL = local_url;
App.instance = new Application.Model();

// Add Viewing Regions
App.addRegions({
	mainRegion    : '#main-region',
	headerRegion  : '#header-region',
	filterRegion  : '#filter-region',
	loadingRegion : '#loading-region'
});

// Start Application
App.on('start', function() {
	
	// Initialise Routers
	App.router          = new AppRouter();
	App.router.steel    = new SteelRouter();
	App.router.user     = new UserRouter();
	App.router.sesssion = new SessionRouter();
	App.router.admin    = new AdminRouter();
	App.router.brick    = new BrickRouter();
	App.router.block    = new BlockRouter();
	App.router.rebar    = new RebarRouter();
	App.router.other    = new OtherRouter();
	App.router.timber   = new TimberRouter();

	// Note: Needs to be removed
	$.ajaxSetup({
	    beforeSend:function(){
	        // show gif here, eg:
	        $("#loading").show();
	    },
	    complete:function(){
	        // hide gif here, eg:
	        $("#loading").hide()
	    },
	    headers: {'x-access-token': localStorage.getItem('token') || ''}
	});

	Backbone.history.start();
});

App.start();

module.exports = App;