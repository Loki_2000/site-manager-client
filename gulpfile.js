'use strict';

const gulp = require('gulp');
const gutil = require('gulp-util');
const hbsfy = require('hbsfy');

const gulpFilter = require('gulp-filter');
const rename = require('gulp-rename');
const path = require('path');
const connect = require('gulp-connect');
const watchify = require('gulp-watchify');
const notifier = require('node-notifier');
const notify = require('gulp-notify');
const browserify = require('browserify');
const fs = require('fs');
const source = require('vinyl-source-stream');
const less = require('gulp-less');
// const shell = require('shelljs');

gulp.task('develop', ['connect', 'browserify'], () => {
	gulp.watch('assets/css/application.less', ['less']);
	gutil.log(gutil.colors.bold.green('watching'));
	const msg = `Server listening on http://localhost:3000`;
	gutil.log(gutil.colors.bgGreen.bold.white(`  ${msg}`));
	// Show mac notification
	notifier.notify({
		message: msg,
		sund: true,
		title: 'Gulp is Ready!'
	})
});

//Todo: Create info file.


//Todo: Setup user configs - this allows us to maintain 
//      a default API URL and use a proxy to override it 
//      during development

// Copy necessary assets to build folder
gulp.task('build', ['browserify', 'js', 'css', 'img', 'fonts'], () => {
	return gutil.log(gutil.colors.bold.green('Assets moved to build folder'));
});

gulp.task('js', () => {
	return gulp.src('assets/js/bundle.js')
		.pipe(rename({basename: 'index'}))
		.pipe(gulp.dest('build/android/www/js/'))
		.pipe(gulpFilter(['**/*.js']))
});

gulp.task('css', () => {
	return gulp.src('assets/css/**/*')
		.pipe(gulp.dest('build/android/www/css/'))
});

gulp.task('img', () => {
	return gulp.src('assets/img/**/*')
		.pipe(gulp.dest('build/android/www/img/'))
});

gulp.task('fonts', () => {
	return gulp.src('assets/fonts/**/*')
		.pipe(gulp.dest('build/android/www/fonts/'))
});

//Todo: Figure out a way to build the apk and upload to cloud storage

// Bundle js files
gulp.task('browserify', () => {
	return browserify({
		extensions: '.js',
		debug: true,
		cache: {},
		packageCache: {},
		fullPaths: true,
		entries: './assets/js/app.js'
	})
	.transform(hbsfy)
	.bundle()
	.on('error', (err) => console.log(err))
	.pipe(source('bundle.js'))
	.pipe(gulp.dest('./assets/js'))
});

//Todo: Setup livereload for CSS and JS

gulp.task('less', function(){
	return gulp.src('assets/css/application.less')
		.pipe(less())
		.pipe(gulp.dest('assets/css'))
});

gulp.task('connect', () => {
	connect.server({
		// Todo: Include source to browse files in devtools
		livereload: true,
		https: false,
		port: 3000	
	});
})












